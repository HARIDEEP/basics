package basics.day41.oops.encapsulation;

public class EncapsulationTest2 {

	/**
	 * 1. Create an object of Person1 and set values for name, age and gender.
	 * Display these values by invoking the getter methods
	 * 
	 * 2. Create another object of Person1 by invoking the parameterized
	 * constructor. Display these details by invoking the toString() method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
