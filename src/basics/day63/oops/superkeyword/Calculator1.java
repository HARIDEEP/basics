package basics.day63.oops.superkeyword;

/**
 * 1. Verify if final keyword can be added between public and abstract. Think
 * through if it is not allowed
 * 
 * 2. Add final keyword for the method add(). Verify the behavior for child
 * class
 * 
 *
 */
public abstract class Calculator1 {
	void add(int a, int b) {
		System.out.println("From abstract class Calculator: " + (a + b));
	}
}
